<?php
include_once $_SERVER['DOCUMENT_ROOT'] .
        '/includes/helpers.inc.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="wrap">
            <div class="container-fluid">
                <form action="?delete" method="post">
                    <div class="d-flex flex-column ">
                        <div class="py-3 d-flex align-items-center justify-content-between border-bottom border-dark">
                            <div class="h4">
                                Product List
                            </div>
                            <div class="d-flex">
                                <a href="add/" >
                                    <button type="button" class="btn btn-light border-bottom border-dark">ADD</button>
                                </a>
                                <button type="submit" class="ml-3 btn btn-light border-bottom border-dark">MASS DELETE</button>
                            </div>
                        </div>
                        <div class="d-flex py-3">
                            <?php foreach ($dvd as $product): ?>
                                <div class="mx-3  d-flex flex-column justify-content-start border border-dark" style="width: 200px;height: 200px">
                                    <div class="m-3 custom-control custom-checkbox d-flex justify-content-start align-items-center">
                                        <div>
                                            <input type="checkbox" class="custom-control-input" 
                                                   name="dvd[]"
                                                   id="product<?php echo htmlout($product['sku']); ?>"
                                                   value="<?php htmlout($product['sku']); ?>">
                                            <label class="custom-control-label" 
                                                   for="product<?php echo htmlout($product['sku']); ?>">
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="">
                                            <?php echo htmlout(strtoupper($product['sku'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout(ucwords($product['name'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout($product['price'] . '$'); ?>
                                        </div>
                                        <div class="d-flex">
                                            <div class="">
                                                <?php echo htmlout(ucfirst($product['attribute'])), ':'; ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php echo htmlout($product['size']); ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php echo htmlout($product['units']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="d-flex py-3 ">
                            <?php foreach ($book as $product): ?>
                                <div class="mx-3  d-flex flex-column justify-content-start border border-dark" style="width: 200px;height: 200px">
                                    <div class="m-3 custom-control custom-checkbox d-flex justify-content-start align-items-center">
                                        <div>
                                            <input type="checkbox" class="custom-control-input" 
                                                   name="book[]"
                                                   id="product<?php echo htmlout($product['sku']); ?>"
                                                   value="<?php htmlout($product['sku']); ?>">
                                            <label class="custom-control-label" 
                                                   for="product<?php echo htmlout($product['sku']); ?>">
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="">
                                            <?php echo htmlout(strtoupper($product['sku'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout(ucwords($product['name'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout($product['price']); ?>
                                            $
                                        </div>
                                        <div class="d-flex">
                                            <div class="">
                                                <?php echo htmlout(ucfirst($product['attribute'])), ':'; ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php echo htmlout($product['weight']); ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php echo htmlout($product['units']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="d-flex py-3 border-bottom border-dark">
                            <?php foreach ($furniture as $product): ?>
                                <div class="mx-3  d-flex flex-column justify-content-center align-items-center border border-dark" style="width: 200px;height: 200px">
                                    <div class="m-3 custom-control custom-checkbox d-flex justify-content-start align-items-center">
                                        <div>
                                            <input type="checkbox" class="custom-control-input" 
                                                   name="furniture[]"
                                                   id="product<?php echo htmlout($product['sku']); ?>"
                                                   value="<?php htmlout($product['sku']); ?>">
                                            <label class="custom-control-label" 
                                                   for="product<?php echo htmlout($product['sku']); ?>">
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="">
                                            <?php echo htmlout(strtoupper($product['sku'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout(ucwords($product['name'])); ?>
                                        </div>
                                        <div class="">
                                            <?php echo htmlout($product['price']); ?>
                                            $
                                        </div>
                                        <div class="d-flex">
                                            <div class="">
                                                <?php echo htmlout(ucfirst($product['attribute'])), ':'; ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php
                                                $attributes = $product['height'] . 'x' . $product['width'] . 'x' . $product['length'];
                                                htmlout($attributes);
                                                ?>
                                            </div>
                                            <div class="ml-1">
                                                <?php echo htmlout($product['units']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="my-3 d-flex justify-content-center">
                            Scandiweb Test assignment
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>