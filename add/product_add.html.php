<?php
include_once $_SERVER['DOCUMENT_ROOT'] .
        '/includes/helpers.inc.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
    </head>
    <body>
        <div class="wrap">
            <div class="container-fluid">
                <!--<form action="" method="post">-->
                <form action="?save" class="needs-validation" novalidate method="post">
                    <div class="d-flex flex-column ">
                        <div class="py-3 d-flex align-items-center justify-content-between border-bottom border-dark">
                            <div class="h4">
                                Product Add
                            </div>
                            <div>
                                <button type="submit" class="btn btn-light border-bottom border-dark">Save</button>
                                <a class="" href="../" >
                                    <button type="button"class="btn btn-light border-bottom border-dark">Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="d-flex border-bottom border-dark">
                            <div class="d-flex py-3 flex-column " style="width: 350px">
                                <div class="my-2 d-flex justify-content-between">
                                    <div class="mt-2">
                                        SKU
                                    </div>
                                    <div class="form">
                                        <input type="text" class="form-control" name="sku" placeholder="ABC123456" required pattern="[A-Z]{3}[0-9]{6}">
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please, submit valid SKU.
                                        </div>
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between">
                                    <div class="mt-2">
                                        Name
                                    </div>
                                    <div class="form">
                                        <input type="text" class="form-control" name="name" placeholder="House Of Cards" required pattern="[A-Za-z' -]+">
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please, provide valid name.
                                        </div>
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between">
                                    <div class="mt-2">
                                        Price (💲)
                                    </div>
                                    <div class="form">
                                        <input type="text" class="form-control" name="price" placeholder="10" required pattern="[0-9]+">
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please, submit some price.
                                        </div>
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between">
                                    <div class="mt-2">
                                        Type Switcher
                                    </div>
                                    <select class="custom-select w-50" name="select_type" onchange="location.href = this.value;" > 
                                        <?php if (isset($get_type)): ?>
                                            <option value=""><?php echo htmlout($type_emj . ' ' . $get_type); ?></option>
                                        <?php else: ?>
                                            <option value="">Select type...</option> 
                                        <?php endif; ?>
                                        <?php foreach ($types as $type): ?>
                                            <option value="?type=<?php echo htmlout($type['name']); ?>">
                                                <?php echo htmlout($type['name']) ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select> 
                                </div>
                                <div class="d-flex flex-column">
                                    <?php if (isset($get_type)): ?>
                                        <?php foreach ($attributes as $attribute): ?>
                                            <div class="my-2 d-flex align-items-center justify-content-between">
                                                <div>
                                                    <?php echo htmlout(ucwords($attribute['attribute']) . ' (' . $attribute['units']) . ')' ?>
                                                </div>
                                                <div class="form">
                                                    <input type="text" class="form-control" placeholder=""
                                                           name="<?php echo htmlout($attribute['attribute']) ?>" required pattern="[0-9]+">
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        Please, enter <?php echo htmlout($attribute['attribute']) ?>.
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        <div class="d-flex justify-content-center small text-secondary">
                                            🙏 <?php echo htmlout($attributes[0]['description']) ?>
                                        </div>
                                        <input type="hidden" name="product_type" value="<?php echo htmlout($get_type); ?>">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="my-3 d-flex justify-content-center">
                            Scandiweb Test assignment
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
        <!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>-->
        <!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>-->
    </body>
</html>
