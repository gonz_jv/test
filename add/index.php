<?php

//connect to DB
include $_SERVER['DOCUMENT_ROOT'] . '/includes/db.inc.php';

// get types from DB 
try {
    $sql = 'SELECT name FROM type';
    $result = $pdo->query($sql);
} catch (PDOException $e) {
    $error = 'Error fetching types: ' . $e->getMessage();
    include $_SERVER['DOCUMENT_ROOT'] . '/includes/error.html.php';
    exit();
}
while ($row = $result->fetch()) {
    $types[] = array(
        'name' => $row['name']);
}

//get product type and refresh "Add" page
if (isset($_GET['type'])) {
    // add emoji to type
    $get_type = $_GET['type'];
    if ($get_type == 'dvd') {
        $type_emj = '📀';
    } elseif ($get_type == 'book') {
        $type_emj = '📚';
    } elseif ($get_type == 'furniture') {
        $type_emj = '🪑';
    }

// get attribute, units, description from DB
    try {
        $sql = 'SELECT attribute,units, description
                FROM attribute
                INNER JOIN type ON type_id=type.id
                WHERE name =' . '"' . $get_type . '"';
        $result = $pdo->query($sql);
    } catch (PDOException $e) {
        $error = 'Error fetching attribute: ' . $e->getMessage();
        include $_SERVER['DOCUMENT_ROOT'] . '/includes/error.html.php';
        exit();
    }
    while ($row = $result->fetch()) {
        $attributes[] = array(
            'attribute' => $row['attribute'],
            'description' => $row['description'],
            'units' => $row['units']);
    }
    include "product_add.html.php";
    exit;
}

//add products to DB
if (isset($_GET['save'])) {
    if ($_POST['product_type'] == 'dvd') {
        try {
            $sql = 'INSERT INTO dvd SET
            sku = :sku,
            name = :name,
            price= :price,
            attribute_id= :attribute_id,
            size = :size';
            $s = $pdo->prepare($sql);
            $s->bindValue(':sku', $_POST['sku']);
            $s->bindValue(':name', $_POST['name']);
            $s->bindValue(':price', $_POST['price']);
            $s->bindValue(':size', $_POST['size']);
            $s->bindValue(':attribute_id', '1');
            $s->execute();
        } catch (PDOException $e) {
            $error = 'Error insert dvd.';
            include $_SERVER['DOCUMENT_ROOT'] . '/includes/error.html.php';
            exit();
        }
    } else if ($_POST['product_type'] == 'book') {
        try {
            $sql = 'INSERT INTO book SET
            sku = :sku,
            name = :name,
            price= :price,
            attribute_id= :attribute_id,
            weight = :weight';
            $s = $pdo->prepare($sql);
            $s->bindValue(':sku', $_POST['sku']);
            $s->bindValue(':name', $_POST['name']);
            $s->bindValue(':price', $_POST['price']);
            $s->bindValue(':weight', $_POST['weight']);
            $s->bindValue(':attribute_id', '2');
            $s->execute();
        } catch (PDOException $e) {
            $error = 'Error insert book.';
            include $_SERVER['DOCUMENT_ROOT'] . '/includes/error.html.php';
            exit();
        }
    } else if ($_POST['product_type'] == 'furniture') {
        try {
            $sql = 'INSERT INTO furniture SET
            sku = :sku,
            name = :name,
            price= :price,
            attribute_id= :attribute_id,
            height= :height,
            width = :width,
            length = :length';
            $s = $pdo->prepare($sql);
            $s->bindValue(':sku', $_POST['sku']);
            $s->bindValue(':name', $_POST['name']);
            $s->bindValue(':price', $_POST['price']);
            $s->bindValue(':height', $_POST['height']);
            $s->bindValue(':width', $_POST['width']);
            $s->bindValue(':length', $_POST['length']);
            $s->bindValue(':attribute_id', '3');
            $s->execute();
        } catch (PDOException $e) {
            $error = 'Error insert furniture.';
            include $_SERVER['DOCUMENT_ROOT'] . '/includes/error.html.php';
            exit();
        }
    }
    header('Location: ..');
    exit();
}
//open "Add" page
include 'product_add.html.php';
?>