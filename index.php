<?php

// Execute delete after 'Mass delete' button is activate
if (isset($_GET['delete'])) {
    include $_SERVER['DOCUMENT_ROOT'] . '/includes/db.inc.php';
//    echo '________';
//    echo $_POST['dvd_delete'];
//    echo '________';
    if (isset($_POST['dvd'])) {
        try {
            $sql = 'DELETE FROM dvd WHERE
            sku = :sku';
            $s = $pdo->prepare($sql);
            foreach ($_POST['dvd'] as $sku) {
                $s->bindValue(':sku', $sku);
                $s->execute();
            }
        } catch (PDOException $e) {
            $error = 'Error delete products.';
            include 'error.html.php';
            exit();
        }
    }
    /* What headers are going to be sent? */
//    var_dump(headers_list());

    if (isset($_POST['book'])) {
        try {
            $sql = 'DELETE FROM book WHERE
            sku = :sku';
            $s = $pdo->prepare($sql);
            foreach ($_POST['book'] as $product) {
                $s->bindValue(':sku', $product);
                $s->execute();
            }
        } catch (PDOException $e) {
            $error = 'Error delete products.';
            include 'error.html.php';
            exit();
        }
    }
    if (isset($_POST['furniture'])) {
        try {
            $sql = 'DELETE FROM furniture WHERE
            sku = :sku';
            $s = $pdo->prepare($sql);
            foreach ($_POST['furniture'] as $product) {
                $s->bindValue(':sku', $product);
                $s->execute();
            }
        } catch (PDOException $e) {
            $error = 'Error delete products.';
            include 'error.html.php';
            exit();
        }
    }
    header('Location: .');
    exit();
}

//connect to DB
include $_SERVER['DOCUMENT_ROOT'] . '/includes/db.inc.php';

//open 'product add' page
if (isset($_GET['add'])) {
    include "./add/index.php";
    exit;
}

// get dvd data
try {
    $sql = 'SELECT sku, name, price, size, attribute,units
          FROM dvd INNER JOIN attribute
          ON attribute_id = attribute.id
          ';
    $result = $pdo->query($sql);
} catch (PDOException $e) {
    $error = 'Error fetching dvd: ' . $e->getMessage();
    include 'error.html.php';
    exit();
}
while ($row = $result->fetch()) {
    $dvd[] = array(
        'sku' => $row['sku'],
        'name' => $row['name'],
        'price' => $row['price'],
        'attribute' => $row['attribute'],
        'units' => $row['units'],
        'size' => $row['size']);
}

// get book data
try {
    $sql = 'SELECT sku, name, price, weight, attribute,units
          FROM book INNER JOIN attribute
          ON attribute_id = attribute.id
          ';
    $result = $pdo->query($sql);
} catch (PDOException $e) {
    $error = 'Error fetching books: ' . $e->getMessage();
    include 'error.html.php';
    exit();
}
while ($row = $result->fetch()) {
    $book[] = array(
        'sku' => $row['sku'],
        'name' => $row['name'],
        'price' => $row['price'],
        'attribute' => $row['attribute'],
        'units' => $row['units'],
        'weight' => $row['weight']);
}

// get furniture data
try {
    $sql = 'SELECT sku, name, price, height, width, length, attribute,units
          FROM furniture INNER JOIN attribute
          ON attribute_id = attribute.id
          ';
    $result = $pdo->query($sql);
} catch (PDOException $e) {
    $error = 'Error fetching furniture: ' . $e->getMessage();
    include 'error.html.php';
    exit();
}
while ($row = $result->fetch()) {
    $furniture[] = array(
        'sku' => $row['sku'],
        'name' => $row['name'],
        'price' => $row['price'],
        'attribute' => $row['attribute'],
        'units' => $row['units'],
        'height' => $row['height'],
        'width' => $row['width'],
        'length' => $row['length']);
}
//open 'Home' page
include 'home.html.php';
